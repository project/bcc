# Blind carbon copy
This is a Drupal module that adds a Bcc address to all emails sent by the 
website.

## Installation
Download and enable as any other Drupal module.

## Configuration
Go to `/admin/config/system/bcc-settings` and enter the email address that 
should receive copies of all site emails.
