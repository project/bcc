<?php

namespace Drupal\Tests\bcc\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Test the module settings page.
 *
 * @group bcc
 */
class SettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The modules to load to run the test.
   *
   * @var array
   */
  protected static $modules = [
    'user',
    'bcc',
  ];

  /**
   * Tests the setting form.
   */
  public function testForm() {
    // Create the user with the appropriate permission.
    $admin_user = $this->drupalCreateUser([
      'administer bcc settings',
    ]);

    // Start the session.
    $session = $this->assertSession();

    // Login as our account.
    $this->drupalLogin($admin_user);

    // Get the settings form path from the route.
    $settings_form_path = Url::fromRoute('bcc.settings');

    // Navigate to the settings form.
    $this->drupalGet($settings_form_path);

    // Assure we loaded settings with proper permissions.
    $session->statusCodeEquals(200);
  }

}
